/**
 * Copyright (c) 2015 Laboratoire de Genie Informatique et Ingenierie de Production - Ecole des Mines d'Ales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Francois Pfister (ISOE-LGI2P) - initial API and implementation
 */
package impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import api.ISocketControler;
import api.ISocketView;

//v6

/**
 *
 * @author cs2015
 *
 */
public class ThreadedClient implements ISocketControler {

	private static final boolean INFO = true;
	
	//protected BufferedReader inBuff;

	//private static final boolean LOCAL = false; // canvas refreshed in local or
												// from thr server respons

	private static final boolean LOG_LOCAL = true;
	private static final boolean LOG_REMOTE = true;

	private ISocketView socketView;

	private String serverName = null;
	private String lhost;
	private int serverPort = -1;
	private String response;

	private PrintWriter out;
	private BufferedReader in;
	private boolean ignoreServer;
		


	@Override
	public void setIgnoreServer(boolean ignoreServer) {
		this.ignoreServer = ignoreServer;
	}

	private Socket clientSocket;

	private List<String> model = new ArrayList<String>();

	private boolean disposed;

	public void setView(ISocketView socketView) {
		this.socketView = socketView;
		viewSetHostAndPort(serverName, serverPort); //az
	}

	@Override
	public boolean checkConnection() {
		boolean result = isConnected();
		if (!result)
			viewClearConnexion();
		return result;
	}

	private void openSocket() throws UnknownHostException, IOException {
		if (isConnected()) {
			info("d�j� connect�");
			return;
		}
		info("ouverture de la socket " + serverName + "-" + serverPort);
		if (serverPort == -1)
			throw new IOException("port error");
		if (serverName == null)
			throw new IOException("host error");
		try {
			clientSocket = new Socket(serverName, serverPort);
			lhost = clientSocket.getLocalAddress().getHostAddress();
			viewSetHostAndPort(serverName, serverPort); //az
		} catch (ConnectException e) {
			error("Connection refus�e ");// + e.toString());
			error("Pas de serveur: " + serverName + "-" + serverPort);
			viewClearConnexion();
			viewSetNoServer();
			in = null;
			out = null;
			throw new IOException("connection error");
		}
		in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		out = new PrintWriter(clientSocket.getOutputStream(), true); // autoflush
		info("Connected to the server");
	}

	private void closeSocket(boolean verbose) {
		if (!isConnected()) {
			if (verbose)
				info("session allready closed");
			return;
		}
		info("closing socket");
		if (clientSocket != null)
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (in != null)
			try {
				in.close();
				in = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (out != null) {
			out.close();
			out = null;
		}
	}

	private void handleServerResponse() {
		if (ignoreServer)
			 return;
		info(response == null ? "session ended" : response);
		if (response == null) {
			closeSocket(true);
			closeHost();
			return;
		}
		clog("from server: " + response);
		if (response.contains("STOP-SERVER OK"))
			info("server will shut down");
		if (response.contains("commande inconnue"))
			info("cmd inconnue: " + response);
		if (response != null && (response.endsWith(" OK") || response.endsWith(" TODO")))
			viewSetResult_(response.substring(response.indexOf("voici la r�ponse:") + 17).trim());
		if (response != null && (response.equals("end") ))
			viewUpdate_();
		else if (response != null) {
			if (response.startsWith("O") || response.startsWith("line") || response.startsWith("rectangle"))
				  addToModel(response);
			
			
			viewSetResult_(response);
		}
	}



	private void closeHost() {
		lhost = null;
		serverName = null;
		serverPort = -1;
	}

	private void readServer() {
		if (!ignoreServer)
		  info("readServer waiting");
		response = null;
		try {
			response = in.readLine(); // bloquant - attendre la r�ponse du
										// serveur
		} catch (IOException e) {
			info("socket ferm�e");
			closeSocket(false);
			closeHost();
			return;
		} catch (Exception e) {
			error("(2) while read response (" + e.toString() + ")");
			closeSocket(false);
			closeHost();
			return;
		}
		if (response == null) {
			info("fin de la connexion");
			closeSocket(false);
			closeHost();
			return;
		}
	}

	private static void delay(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getResponse() {
		return response;
	}

	@Override
	public void disconnect(boolean verbose) {
		if (isConnected())
			closeSocket(verbose);
		else if (verbose)
			clog("allready disconnected !");
		closeHost();
		socketView.redrawGui();
	}

	@Override
	public boolean connect(String host, int port) {
		if (isConnected()) {
			clog("allready connected !");
			return true;
		}
		model.clear();
			new SessionThread(host, port).start();
			delay(100);
			return isConnected();
		
		
	}

	private boolean isConnected() {
		boolean result = clientSocket != null && !clientSocket.isClosed() && clientSocket.isConnected()
				&& (socketView == null || !socketView.isDisposed());
		if (!result) {
			if (clientSocket == null)
				clog("clientSocket == null");
			if (clientSocket != null && clientSocket.isClosed())
				clog("clientSocket.isClosed()");
			if (clientSocket != null && !clientSocket.isConnected())
				clog("!clientSocket.isConnected()");
			if (socketView != null && socketView.isDisposed())
				clog("socketView.isDisposed()");
		}
		return result;
	}

	private boolean openConnection() {
		try {
			openSocket();
			return true;
		} catch (UnknownHostException e) {
			error("serveur inconnu: " + e.getMessage());
		} catch (IOException e) {
			error("erreur de connexion");
		} catch (Exception e) {
			error("(connect 3) " + e.toString());
		}
		return false;
	}

	@Override
	public void send(String host, int port, String cmd) {
		if (cmd != null && !cmd.isEmpty()) {
			if (isConnected()) {
				if (!serverName.equals(host) || serverPort != port)
					disconnect(true);
			}
			if (!isConnected())
				connect(host, port);
			if (isConnected())
				send(cmd);
		} else
			clog("command is empty !");
	}

	@Override
	public boolean send(String cmd) {
		if (cmd != null && !cmd.isEmpty()) {
			info("send " + serverName + ":" + serverPort + "[" + cmd + "]");
			if (!isConnected())
				connect(serverName, serverPort);
			if (isConnected()) {
				if (out==null){
				  error("out is null in send(String cmd)");	
				  return false;
				}	
				out.println(cmd); //server will call back the command
				return true;
			} else
				error("no session");
		} else
			clog("command is empty !");
		return false;
	}


	class SessionThread extends Thread {
		public SessionThread(String host, int port) {
			if (isConnected())
				disconnect(true);
			serverName = host;
			serverPort = port;
		}

		@Override
		public void run() {
			if (openConnection()) {
				while (checkConnection()) {
					/*
					 * if (userIn_ != null) // dans ce cas, necessit� de g�rer
					 * une // t�che // parall�le pour ne pas bloquer les // push
					 * du serveur avec l'attente sur le // clavier
					 * readConsole_();
					 */
					readServer();
					handleServerResponse();
				}
				disconnect(false);
			}
		}
	}

	@Override
	public String[] getUsage() {
		int i = 0;
		List<String> usags = new ArrayList<String>();
		usags.add("stop-server => arr�t du serveur");
		usags.add("bye => fin de la session");
		usags.add("connect host port => d�but de session");
		usags.add("quit => fin de l'application");
		String[] result = new String[usags.size()];
		for (String usag : usags)
			result[i++] = usag;
		return result;
	}

	private void viewSetHostAndPort(String host, int port) {
		if (socketView != null)
			socketView.setConnection(host, port);
		else{
			if (host!=null)
			  clog("connexion depuis " + host + "." + port + " �tablie");
			else
				 clog("pas de connextion");
		}
	}

	private void viewClearConnexion() {
		if (socketView != null)
			socketView.clearConnection();
		else
			clog("plus de connexion ");
	}

	private void viewSetNoServer() {
		if (socketView != null)
			socketView.setNoServer();
		else
			clog("plus de serveur ");
	}

	private void viewSetResult_(String result) {
		if (socketView != null)
			socketView.setResult(result);
		else
			clog("r�sultat " + serverName + "." + serverPort + " [" + result + "]");
	}

	
	private void viewUpdate_() {
		if (socketView != null)
			socketView.redrawModel(false);
		else
			clog("viewUpdate");
	}
	
	
	private void clog(String mesg) {
		if (LOG_REMOTE) {
			if (socketView != null && socketView.isReady())
				socketView.log(mesg);
		}

		if (LOG_LOCAL) {

			System.out.println(mesg);
		}
	}

	private void error(String mesg) {
		viewError(mesg);
	}

	private void info(String mesg) {
		viewInfo(mesg);
	}

	private void viewError(String mesg) {
		if (socketView != null)
			socketView.error(mesg);
		else
			System.err.println(mesg);
	}

	private void viewInfo(String mesg) {
		if (INFO) {
			if (socketView != null)
				socketView.info(mesg);
			else
				clog("i__" + mesg);
		}
	}

	@Override
	public List<String> getModel() {
		return model;
	}

	private void addToModel(String cmd) {
		clog("added to model: " + response);
		model.add(cmd);
	}

	public static void main(String[] args) throws IOException {
		ISocketControler client = new ThreadedClient();// null);
		System.out.println(client.getUsage());
		BufferedReader userIn = new BufferedReader(new InputStreamReader(System.in));
		// client.setConsole(userIn);
		client.connect(DEFAULT_HOST, DEFAULT_PORT);
		while (client.checkConnection())
			delay(1000);
		System.out.println("Termin� ! ");
	}

	@Override
	public void unContribute(ISocketView view) {
		clog("uncontribute " + view.getClass().getSimpleName());
		this.socketView = null;
	}

	@Override
	public void contribute(ISocketView view) {
		clog("contribute " + view.getClass().getSimpleName());
		// this.socketView = view;
	}

	@Override
	public void setReady() {
		clog("__ready__" +socketView.getClass().getSimpleName());
		viewSetHostAndPort(serverName, serverPort);//az
		socketView.redrawGui();
		socketView.redrawModel(true);	
	}

	@Override
	public void dispose() {
		disposed = true;
		System.out.println("must close " + this.getClass().getSimpleName());
	}

	@Override
	public void viewDisposed() {
		clog("view disposed for " + this.getClass().getSimpleName());
		
	}

	@Override
	public void setConsole(BufferedReader userIn) {
		// TODO Auto-generated method stub
		
	}

}
