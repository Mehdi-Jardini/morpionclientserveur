/**
 * Copyright (c) 2015 Laboratoire de Genie Informatique et Ingenierie de Production - Ecole des Mines d'Ales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Francois Pfister (ISOE-LGI2P) - initial API and implementation
 */
package api;

import java.io.BufferedReader;
import java.util.List;

//v6


/**
 *
 * @author cs2015
 *
 */
public interface ISocketControler {
	
	static final int DEFAULT_PORT = 8051;
	static final String DEFAULT_HOST = "127.0.0.1";
	String getResponse();
	boolean send(String cmd);
	void send(String host, int port, String cmd);
	String[] getUsage();
	void disconnect(boolean verbose);
	boolean connect(String host, int port);
	List<String> getModel();
	boolean checkConnection();
	void setConsole(BufferedReader userIn);
	void setView(ISocketView view);
	void unContribute(ISocketView view);
	void contribute(ISocketView view);
	void viewDisposed();
	void setIgnoreServer(boolean value);
	void setReady();
	void dispose();
	
}
