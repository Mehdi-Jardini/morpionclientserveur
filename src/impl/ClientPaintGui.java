/**
 * Copyright (c) 2015 Laboratoire de Genie Informatique et Ingenierie de Production - Ecole des Mines d'Ales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Francois Pfister (ISOE-LGI2P) - initial API and implementation
 */

package impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import api.ISocketControler;
import api.ISocketView;

//v6

/**
 * 
 * @author cs2015
 * 
 *
 */
public class ClientPaintGui implements ISocketView, DisposeListener, PaintListener {
	// private Display display;
	protected Shell shell;
	private Canvas canvas;

	private Group groupPalette;
	private Text logText;
	private Text textLocalHost;
	private Text textLocalPort;
	private Button btnConnect;
	private Button btnDisconnect;
	private Label lblHost;
	private Label lblPort;
	private Text textResult;

	private boolean connected;
	private boolean disposed;
	private ISocketControler controler;
	protected Point mouseUp;
	private Point mouseDown;
	private Point mouseEnter;
	private boolean drag;

	private static final boolean LOG_LOCAL = true;
	private static final boolean LOG_REMOTE = true;
	private String host;
	private int port = -1;
	private boolean buttonClearOn=false;

	private List<String> model = new ArrayList<String>();
	private boolean closed;
	private boolean ready;
	private Button btnClearLocal;
	private Button btnIgnoreServer;

	private Image board;
	private Image blueX;
	private Image blueCircle;
	private Image redX;
	private Image redCircle;
	private boolean redrawthis=false;
	
	private ArrayList<String> figure = new ArrayList<String>();
	
	
	private String[] spaces= new String[9];
	
	private boolean yourTurn = true;
	private boolean enemyWon = false;
	private boolean won = false;
	private boolean circle=true;
	
	private int firstSpot = -1;
	private int secondSpot = -1;
	
	private String wonString = "Vous avez gagn�!";
	private String enemyWonString = "votre adversaire a gagn�!";
	
	private Label lblScore;
	private static int score=0;
	
	private int lengthOfSpace = 128;
	
	private int[][] wins = new int[][] { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 }, { 0, 4, 8 }, { 2, 4, 6 } };
	
	
	private void setReady(boolean value) {
		if (!ready && value) {
			ready = true;
			controler.setReady();
		} else if (ready && !value) {
			//controler.setReady(false);
			ready = false;
		}
	}

	@Override
	public void open() {
		Display display = Display.getDefault();
		disposed = false;
		closed = false;
		createContents();
		shell.open();
		shell.layout();
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event event) {
				clog("dispose shell " + ClientPaintGui.class.getSimpleName());
				controler.viewDisposed();
				shell.dispose();
			}
		});
		setReady(true);
		while (!shell.isDisposed() && !closed) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		setReady(false);
		if (!shell.isDisposed())
			shell.dispose();
	}
	

	public void open___() {
		final Display display = Display.getDefault();
		display.asyncExec(new Runnable() {
			@Override
			public void run() {
				disposed = false;
				closed = false;
				createContents();
				shell.open();
				shell.layout();
				shell.addListener(SWT.Close, new Listener() {
					@Override
					public void handleEvent(Event event) {
						clog("dispose shell " + ClientPaintGui.class.getSimpleName());
						controler.viewDisposed();
						shell.dispose();
					}
				});
				setReady(true);
				while (!shell.isDisposed() && !closed) {
					if (!display.readAndDispatch()) {
						display.sleep();
					}
				}
				setReady(false);
				if (!shell.isDisposed())
					shell.dispose();
			}
		});
		
		while (!disposed  && !closed)
			delay(100);
		
	}

	/*
	 * public ClientPaintGui(ISocketControler c) { super(); socketControler =
	 * c;// socketControler.setView(this); }
	 */

	public ClientPaintGui() {
		super();
	}

	@Override
	public void setControler(ISocketControler c) {
		this.controler = c;
		this.controler.setView(this);
		host = controler.DEFAULT_HOST;
		port = controler.DEFAULT_PORT;
	}

	private static void delay(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void sendCommand() {
		//String cmdText = textCmd.getText();
		//controler.send(host, port, cmdText);
		redrawModel(true);
	}

	protected void addObject() {
		
		String cmd = "";
		
		int x = mouseEnter.x;
		int y = mouseEnter.y;
		
		//circle= false;
		
		cmd="X " + x + " " + y + " " ;
		/*if (btnRectangle.getSelection())
			cmd = "rectangle " + mouseDown.x + " " + mouseDown.y + " " + mouseUp.x + " " + mouseUp.y;
		else if (btnCircle.getSelection()) {
			int[] args = toCircle(mouseDown.x, mouseDown.y, mouseUp.x, mouseUp.y);
			cmd = "circle " + args[0] + " " + args[1] + " " + args[2];
		} else
			cmd = "line " + mouseDown.x + " " + mouseDown.y + " " + mouseUp.x + " " + mouseUp.y;
		getHost();
		controler.send(host, port, cmd);
		textCmd.setText(cmd);*/
		if (!connected)
		  redrawModel(true);
		
		getHost();
		controler.send(host, port, cmd);
		//textCmd.setText(cmd);*/
	}

	private int[] toCircle(int x, int y, int x2, int y2) {
		int[] result = new int[3];
		Integer w = (x2 - x) * 2;
		Integer h = (y2 - y) * 2;
		int r = Math.max(w, h);
		x = x - r / 2;
		y = y - r / 2;
		result[0] = x;
		result[1] = y;
		result[2] = r;
		return result;
	}

	@Override
	public void redrawModel(boolean local) {
		boolean tb=false;
		if (!ready) {
			clog("refresh while not ready");
			return;
		}
		clog("<redrawModel> "+(local?"local":"server"));
		synchronized (controler) { // avoids
									// ConcurrentModificationException
			model = new ArrayList<String>();
			model.addAll((List<String>) controler.getModel());
		}
		redraw();
	}
	

	private void clearLocal() {
		buttonClearOn=true;
		initializeModel();
		model = new ArrayList<String>();
		figure= new ArrayList<String>();
		for (int i=0; i<9;i++)
			spaces[i]="0";
		redraw();
	}

	private void redraw() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				if (ready) {
					canvas.redraw();
					String content = "";
					for (String line : model)
						content += line + "\r\n";
					logText.setText(content);
				}
			}
		});
	}

	private void clear() {
		logText.setText("");
		textResult.setText("");
		//textCmd.setText("");
	}

	private void getHost() {
		host = textLocalHost.getText();
		try {
			port = Integer.parseInt(textLocalPort.getText());
		} catch (Exception e) {
			port = -1;
		}
	}

	private void disconnect() {
		clog("disconnect");
		controler.disconnect(true);
	}

	@Override
	public boolean isDisposed() {
		return disposed;
	}

	@Override
	public void widgetDisposed(DisposeEvent e) {
		disposed = true;
		controler = null;
		setReady(false);
	}

	@Override
	public void clog(String mesg) {
		if (LOG_LOCAL)
			System.out.println(mesg);
	}

	@Override
	public void error(String mesg) {
		if (ready)
			err(mesg);
		else
			clog("error:" + mesg);
	}

	@Override
	public void info(String mesg) {
		if (ready)
			log(mesg);
		else {
			clog("info:" + mesg);
		}
	}

	@Override
	public void setResult(String result) {
		if (ready)
			setRes(result);
		else {
			clog("result:" + result);
		}
	}

	@Override
	public void clearConnection() {
		this.host = null;
		this.port = -1;
		connected = false;
		// response("[connection closed]");
		initUi();
	}

	/*
	 * @Override public void setConnection(final String localhost, final int
	 * localport) { this.connected = true; initUi(); }
	 * 
	 */

	@Override
	public void setConnection(String host, int port) {
		
		this.host = host;
		this.port = port;
		// response(host==null?"[disconnected]":("[connected:" + host + "." +
		// port + "]"));
		this.connected = host == null ? false : true;
		initUi();
		if (connected) {
			String[] usage = controler.getUsage();
			String info = "";
			for (String usag : usage)
				info += usag + "\r\n";
			info(info);
		}
	}

	@Override
	public void setNoServer() {
		err("no server available");
	}

	public static void main(String[] args) {
		try {
			ClientPaintGui window = new ClientPaintGui();
			window.setControler(new ThreadedClient());
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	@Override
	public void close() {
		closed = true;
	}

	@Override
	public boolean isReady() {
		return ready;
	}

	/**********************/

	/**
	 * @wbp.parser.entryPoint
	 */
	protected void createContents() {
		// shell = new Shell();
		

		for( int nb=0;nb<spaces.length;nb++)
			spaces[nb]="0";
			
		shell = new Shell(Display.getDefault(), SWT.CLOSE);

		shell.setBackground(SWTResourceManager.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
		shell.setSize(450, 710);
		shell.setText("Whiteboard client");

		canvas = new Canvas(shell, SWT.NONE);
		canvas.setBounds(10, 10, 424, 424);

		board =new Image(Display.getDefault(),"C:/Users/User/Projet1/Client/src/res/board.png");
		blueX =new Image(Display.getDefault(),"C:/Users/User/Projet1/Client/src/res/blueX.png");
		redX =new Image(Display.getDefault(),"C:/Users/User/Projet1/Client/src/res/redX.png");
		blueCircle =new Image(Display.getDefault(),"C:/Users/User/Projet1/Client/src/res/blueCircle.png");
		redCircle =new Image(Display.getDefault(),"C:/Users/User/Projet1/Client/src/res/redCircle.png");
		
		
		groupPalette = new Group(shell, SWT.NONE);
		groupPalette.setBounds(285, 232, 139, 113);

		logText = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		logText.setBounds(10, 440, 269, 167);

		textLocalHost = new Text(shell, SWT.BORDER);
		textLocalHost.setText(ISocketControler.DEFAULT_HOST);
		textLocalHost.setBounds(47, 622, 76, 21);

		textLocalPort = new Text(shell, SWT.BORDER);
		textLocalPort.setText(Integer.toString(ISocketControler.DEFAULT_PORT));
		textLocalPort.setBounds(165, 622, 76, 21);

		btnConnect = new Button(shell, SWT.NONE);
		btnConnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				getHost();
				controler.connect(host, port);
				delay(100);
				mouseDown = null;
				mouseUp = null;
				canvas.redraw();
				
				
			}
		});
		btnConnect.setBounds(47, 649, 75, 25);
		btnConnect.setText("connect");

		btnDisconnect = new Button(shell, SWT.NONE);
		btnDisconnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				disconnect();
			}
		});
		btnDisconnect.setBounds(165, 649, 75, 25);
		btnDisconnect.setText("disconnect");

		lblHost = new Label(shell, SWT.NONE);
		lblHost.setBounds(10, 625, 32, 15);
		lblHost.setText("host");

		lblPort = new Label(shell, SWT.NONE);
		lblPort.setBounds(126, 625, 32, 15);
		lblPort.setText("port");

		textResult = new Text(shell, SWT.BORDER | SWT.READ_ONLY);
		textResult.setBounds(267, 622, 139, 21);

		String atStart = "";
		if (controler != null) {
			String[] usage = controler.getUsage();
			for (String line : usage)
				atStart += line + "\r\n";
		}

		logText.setText(atStart);
		logText.setTopIndex(logText.getLineCount() - 1);

		btnClearLocal = new Button(shell, SWT.NONE);
		btnClearLocal.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				clearLocal();
			}

		});
		btnClearLocal.setBounds(267, 649, 75, 25);
		btnClearLocal.setText("Clear game");
		
		btnIgnoreServer = new Button(shell, SWT.CHECK);
		btnIgnoreServer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
                  ignoreServer(btnIgnoreServer.getSelection());
			}
		});
		btnIgnoreServer.setBounds(341, 591, 93, 16);
		btnIgnoreServer.setText("Ignore Server");
		
		lblScore = new Label(shell, SWT.NONE);
		lblScore.setBounds(332, 487, 55, 15);
		lblScore.setText("Score :"+score);

		
		canvas.addPaintListener(this); // notifie lorsque le canvas doit �tre
										// redessin�, appelle paintControl

		canvas.addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(Event event) {
				mouseEnter = new Point (event.x,event.y);
				String figurette="";
				figurette = "O " + event.x + " " + event.y + " " ;
				if(connected)
				figure.add(figurette);
				canvas.redraw();
				addObject();
				buttonClearOn=false;
				
				
			}
		});

		canvas.addListener(SWT.MouseMove, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (drag) {
					mouseUp = new Point(event.x, event.y);
					canvas.redraw();
					//addObject();
				}
			}
		});
		canvas.addListener(SWT.MouseUp, new Listener() {
			@Override
			public void handleEvent(Event event) {
				drag = false;
				mouseUp = new Point(event.x, event.y);
				//addObject();
			}
		});
	}

	protected void ignoreServer(boolean value) {
		controler.setIgnoreServer(value);
	}

	@Override
	public void log(final String mesg) {

		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				if (!ready)
					return;
				logText.setText(logText.getText() + "\r\n" + ": " + mesg);
				logText.setTopIndex(logText.getLineCount() - 1);
			}
		});
	}

	private void err(final String mesg) {

		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				if (!ready)
					return;
				logText.setText(logText.getText() + "\r\n" + "error: " + mesg);
				logText.setTopIndex(logText.getLineCount() - 1);
			}
		});
	}

	private void setRes(final String result) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				if (!ready)
					return;
				if (result.startsWith("O")) {
					//textCmd.setText(result);
					canvas.redraw();
				} else
					textResult.setText(result);
			}
		});
	}

	@Override
	public void redrawGui() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				// logText.setText("");
				// textResult.setText("");
				if (!ready)
					return;/*
							 * if (host!=null){
							 * textLocalPort_.setText(Integer.toString(port));
							 * textLocalHost_.setText(host); } else{
							 * textLocalPort_.setText("");
							 * textLocalHost_.setText(""); }
							 */
			}
		});
		initUi();
	}

	private void initUi() {
        boolean tb = true;
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				if (!ready)
					return;
				if (connected) {
					shell.setBackground(SWTResourceManager.getColor(255, 240, 245));
				} else {
					shell.setBackground(SWTResourceManager.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
				}
				textLocalHost.setText(host==null?"127.0.0.1":host);
				textLocalPort.setText(Integer.toString(port==-1?8051:port));
			}
		});
	}

	/**
	 * appel� lorsque le canvas doit �tre redessin�
	 */
	@Override
	public void paintControl(PaintEvent e) {
		if (!ready)
			return;

		
		Image img;
		
		e.gc.drawImage(board, 5, 5);
		//if(connected)
		for(String ligne:figure)
		{
			String [] arg=ligne.split(" ");
			String forme = (String)arg[0];
			Integer x = Integer.parseInt(arg[1]);
			Integer y = Integer.parseInt(arg[2]);
			//e.gc.drawImage(redX, (x % 3) * Lenghtofspace + 10 * (x % 3), (int) (y / 3) * Lenghtofspace + 10 * (int) (y / 3));
			
				
			textResult.setText("la valeur est : "+spaces[0]);
			if(y>=0 && y <=128)
				{
					if( x>=0 && x <=128 && spaces[0]!="O" )
					{	
						e.gc.drawImage(redX, 27, 27);
						spaces[0]="X";
						
						
						}
						else 
							if ( x>=148 && x <=266 && spaces[1]!="O" )
								{
								spaces[1]="X";
									e.gc.drawImage(redX, 167, 27);
								}
							else
								if( x>=290 && x <=414 && spaces[2]!="O")
									{
									spaces[2]="X";
										e.gc.drawImage(redX, 307, 27);
									}
				}
			else 
				if (y>=148 && y <=266)
				{
						if( x>=0 && x <=128  && spaces[3]!="O")
						{
						
							spaces[3]="X";
						e.gc.drawImage(redX, 27, 167);}
							else 
								if ( x>=148 && x <=266 && spaces[4]!="O")
									{
									
										spaces[4]="X";
									e.gc.drawImage(redX, 167, 167); }
								else
									if( x>=290 && x <=414 && spaces[5]!="O" )
										{
										spaces[5]="X";
										e.gc.drawImage(redX, 307, 167);}
				}
			else
				if( y>=290 && y <=414 )
				{
					if( x>=0 && x <=128 && spaces[6]!="O" )
					{
					spaces[6]="X";
					e.gc.drawImage(redX, 27, 307);}
					
						else 
							if ( x>=148 && x <=266 && spaces[7]!="O")
								{
								spaces[7]="X";
								e.gc.drawImage(redX, 167, 307);}
							else
								if( x>=290 && x <=414 && spaces[8]!="O" )
									{
									spaces[8]="X";
									e.gc.drawImage(redX, 307, 307);}
				}
					
			}
		

		
			
			
		
		/*if (mouseDown != null && mouseUp != null) {
			if (btnRectangle.getSelection()) {
				Integer w = mouseUp.x - mouseDown.x;
				Integer h = mouseUp.y - mouseDown.y;
				e.gc.drawRectangle(mouseDown.x, mouseDown.y, w, h);
			} else if (btnCircle.getSelection()) {
				int[] c = toCircle(mouseDown.x, mouseDown.y, mouseUp.x, mouseUp.y);
				e.gc.drawOval(c[0], c[1], c[2], c[2]);
			} else
				e.gc.drawLine(mouseDown.x, mouseDown.y, mouseUp.x, mouseUp.y);
		}*/

		if (controler != null)
		{	
			
			for (String line : model) {
				try {
					String data ="O";
					textResult.setText("�a marche");
					String[] args = line.split(" ");
					String shape = (String) args[0];
					Integer x = Integer.parseInt(args[1]);
					Integer y = Integer.parseInt(args[2]);
					//Integer x1 = Integer.parseInt(args[3]);
					//Integer y1 = args.length == 5 ? Integer.parseInt(args[4]) : 0;
					
					if (shape.equals("O"))
					{
						if(y>=0 && y <=128)
						{
							if( x>=0 && x <=128 && spaces[0]!="X")
							{
									spaces[0]="O";
									e.gc.drawImage(blueCircle, 27, 27);
									
								
							}
								else 
									if ( x>=148 && x <=266 && spaces[1]!="X")
										{
												spaces[1]="O";
												e.gc.drawImage(blueCircle, 167, 27);
												
										}
									else
										if( x>=290 && x <=414  && spaces[2]!="X")
											{
												
											
												spaces[2]="O";
												e.gc.drawImage(blueCircle, 307, 27);
													
											}
						}
					else 
						if (y>=148 && y <=266)
						{
								if( x>=0 && x <=128 && spaces[3]!="X")
								{
									spaces[3]="O";
									e.gc.drawImage(blueCircle, 27, 167);
									
								}
									else 
										if ( x>=148 && x <=266 && spaces[4]!="X" )
											{
											spaces[4]="O";
												e.gc.drawImage(blueCircle, 167, 167);
												}
										else
											if( x>=290 && x <=414 && spaces[5]!="X" )
												{
												spaces[5]="O";
													e.gc.drawImage(blueCircle, 307, 167);}
						}
					else
						if( y>=290 && y <=414 )
						{
							if( x>=0 && x <=128 && spaces[6]!="X" )
							{
								spaces[6]="O";
								e.gc.drawImage(blueCircle, 27, 307);}
								else 
									if ( x>=148 && x <=266 && spaces[7]!="X" )
										{
										spaces[7]="O";
											e.gc.drawImage(blueCircle, 167, 307);}
									else
										if( x>=290 && x <=414 && spaces[8]!="X")
											{
											spaces[8]="O";
											e.gc.drawImage(blueCircle, 307, 307);}
						}
					
					/*if (shape.equals("circle")) {
						e.gc.drawOval(x, y, x1, x1);
					} else if (shape.equals("rectangle")) {
						Integer w = x1 - x;
						Integer h = y1 - y;
						e.gc.drawRectangle(x, y, w, h);
					} else if (shape.equals("line"))
						e.gc.drawLine(x, y, x1, y1);*/
						e.gc.setLineWidth(8);
						if(checkForWin())
						{
							e.gc.drawString("Vous avez gagn� " , 207, 207);
							//e.gc.drawText();
							e.gc.drawLine(firstSpot % 3 * lengthOfSpace + 10 * firstSpot % 3 + lengthOfSpace / 2, (int) (firstSpot / 3) * lengthOfSpace + 10 * (int) (firstSpot / 3) + lengthOfSpace / 2, secondSpot % 3 * lengthOfSpace + 10 * secondSpot % 3 + lengthOfSpace / 2, (int) (secondSpot / 3) * lengthOfSpace + 10 * (int) (secondSpot / 3) + lengthOfSpace / 2);
						
							score++;
							lblScore.setText("Score : "+score);
						}
						else
						if(checkForEnemyWin()){
							
							e.gc.drawString("Votre adversaire a gagn� " , 207, 207);
							//e.gc.drawText();
							e.gc.drawLine(firstSpot % 3 * lengthOfSpace + 10 * firstSpot % 3 + lengthOfSpace / 2, (int) (firstSpot / 3) * lengthOfSpace + 10 * (int) (firstSpot / 3) + lengthOfSpace / 2, secondSpot % 3 * lengthOfSpace + 10 * secondSpot % 3 + lengthOfSpace / 2, (int) (secondSpot / 3) * lengthOfSpace + 10 * (int) (secondSpot / 3) + lengthOfSpace / 2);
						}
						else
							if(checkforDraw())
							e.gc.drawString("Egalit� " , 207, 207);
						
				}
				}catch (Exception e2) {
					error("err while parsing cmd");
				}
			}
		}
	}
	
	private boolean checkforDraw() {
		
		boolean etat=false;
		int i;
		for (i= 0; i<9 && spaces[i]!="0";i++);
		if (i==9)
			if(!checkForWin() && !checkForEnemyWin())
				etat= true;

		return etat;
	}

	private boolean checkForWin() {
		for (int i = 0; i < wins.length; i++) {		 
				if (spaces[wins[i][0]] == "X" && spaces[wins[i][1]] == "X" && spaces[wins[i][2]] == "X") {
					textResult.setText("vous avez gagn�");
					firstSpot = wins[i][0];
					secondSpot = wins[i][2];
					won = true;
				}
		}
		return won;
	}
	
	private boolean checkForEnemyWin() {
		for (int i = 0; i < wins.length; i++) {
				if (spaces[wins[i][0]] == "O" && spaces[wins[i][1]] == "O" && spaces[wins[i][2]] == "O") {
					textResult.setText("L'enemi a gagn�");
					firstSpot = wins[i][0];
					secondSpot = wins[i][2];
					enemyWon = true;
				}
		}
		return enemyWon;
	}
	
	private void fillSpace(String data, int i){
		if (spaces[i]!="X" && spaces[i]!="O")
			spaces[i]=data;
		
	}
	
	private void initializeModel(){
		
		model = new ArrayList<String>();
		
	}
}
